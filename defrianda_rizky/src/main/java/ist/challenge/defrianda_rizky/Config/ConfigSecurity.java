@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class KonfigurasiSecurity extends WebSecurityConfigurerAdapter{
    private static final String SQL_LOGIN
            = "SELECT username,password, enable " +
            "FROM s_users WHERE username = ?";

    private static final String SQL_PERMISSION
            = "SELECT u.username, r.nama as authority " +
            "FROM s_users u " +
            "JOIN s_user_role ur on u.id = ur.id_user " +
            "JOIN s_roles r on ur.id_role = r.id " +
            "WHERE u.username = ?";

    @Autowired
    private DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{

        //setting security non database
        auth
                .inMemoryAuthentication()
                .withUser("ciazhar")
                .password("123")
                .roles("apa");

        ///Setting security database
        /*auth
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(SQL_LOGIN)
                .authoritiesByUsernameQuery(SQL_PERMISSION);*/
    }

    ///konfigurasi web mana yg boleh diakses admin staf user dll
    protected void configure(HttpSecurity http) throws Exception{
        http
                .authorizeRequests()
                .antMatchers("/css/**","/js/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/")
                .permitAll()
                .and()
                .logout();
    }
}