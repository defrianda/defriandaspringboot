package ist.challenge.defrianda_rizky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DefriandaRizkyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DefriandaRizkyApplication.class, args);
	}

}
